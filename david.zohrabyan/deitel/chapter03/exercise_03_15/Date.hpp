/// The heading file of Date

class Date
{
public:
    /// Constructor initializing
    Date(int month, int day, int year);
    
    /// function set Month
    void setMonth(int month);
    
    /// function set Day
    void setDay(int day);
    
    /// function set Year
    void setYear(int year);
    
    /// function get Month
    int getMonth();
    
    /// function get Day
    int getDay();
    
    /// function get Year
    int getYear();
    
    /// function display the date
    void displayDate();
    
private:
    /// member variable of Month
    int month_ ;
    
    /// member variable of Day    
    int day_ ;
    
    /// member variable of Year    
    int year_ ;
 
};
