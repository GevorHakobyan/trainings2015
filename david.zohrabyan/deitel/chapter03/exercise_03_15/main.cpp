/// Date class testing
#include <iostream>
#include "Date.hpp" /// Implementation of design’s Date

int
main()
{
    Date date1(5, 25, 2015); /// creates the first object of a class Date and gives parameter
    Date date2(-4, 45, 2016); /// creates the second object of a class Date and gives parameter
    Date date3(13, 40, 2045); /// creates the third object of a class Date and gives parameter
    
    date1.displayDate();
    date2.displayDate();
    date3.displayDate();
    
    return 0;
}
