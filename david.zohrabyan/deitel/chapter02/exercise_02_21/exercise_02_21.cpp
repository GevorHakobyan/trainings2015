/// This program prints a rectangle,an oval,an arrow and a rhombus
#include <iostream> /// standard input output
 
/// function main begins program execution
int main() 
{
    /// prints a rectangle,an oval,an arrow and a rhombus
    std::cout << " xxxxxxxxx       x x x               x             x      \n";   
    std::cout << " x       x     x       x           x x x         x   x    \n";
    std::cout << " x       x    x         x        x x x x x      x     x   \n";    
    std::cout << " x       x    x         x            x         x       x  \n";
    std::cout << " x       x    x         x            x        x         x \n"; 
    std::cout << " x       x    x         x            x         x       x  \n";
    std::cout << " x       x    x         x            x          x     x   \n";
    std::cout << " x       x     x       x             x           x   x    \n";
    std::cout << " xxxxxxxxx       x x x               x             x      \n";

    return 0; /// program ended successfully
} /// end function main
