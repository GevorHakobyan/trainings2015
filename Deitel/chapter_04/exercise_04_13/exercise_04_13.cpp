#include <iostream>

int
main()
{
    double s, oil, x = 0, y = 0, m, n;
    
    std::cout << "Enter distance(non-positive number to quit): ";
    std::cin  >> s;   

    while (s > 0){
        std::cout << "Enter consumption of gazoline: ";
        std::cin  >> oil;
        if (oil > 0){
            m = s / oil;
            std::cout << "Mile/gallon for this refueling: " << m ;
            x += s;
            y += oil;
            n = x / y;
            std::cout << "\nSummary of values mile/gallon: " << n ; 
        }
        std::cout << "\n\nEnter distance(non-positive number to quit): ";
        std::cin  >> s;     
    }
             
    return 0;
}
