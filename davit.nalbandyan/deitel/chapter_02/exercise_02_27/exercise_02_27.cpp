#include <iostream>

int
main()
{
    std::cout << "A static cast is " << static_cast<int>('A') << std::endl;
    std::cout << "B static cast is " << static_cast<int>('B') << std::endl;
    std::cout << "C static cast is " << static_cast<int>('C') << std::endl;
    std::cout << "a static cast is " << static_cast<int>('a') << std::endl;
    std::cout << "b static cast is " << static_cast<int>('b') << std::endl;
    std::cout << "c static cast is " << static_cast<int>('c') << std::endl;
    std::cout << "0 static cast is " << static_cast<int>('0') << std::endl;
    std::cout << "1 static cast is " << static_cast<int>('1') << std::endl;
    std::cout << "2 static cast is " << static_cast<int>('2') << std::endl;
    std::cout << "$ static cast is " << static_cast<int>('$') << std::endl;
    std::cout << "* static cast is " << static_cast<int>('*') << std::endl;
    std::cout << "+ static cast is " << static_cast<int>('+') << std::endl;
    std::cout << "/ static cast is " << static_cast<int>('/') << std::endl;
    std::cout << "space static cast is " << static_cast<int>(' ') << std::endl;

    return 0;
}
