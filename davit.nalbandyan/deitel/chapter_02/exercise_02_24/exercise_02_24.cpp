#include <iostream>

int
main()
{
    int a;
    std::cout << "Enter number: ";
    std::cin >> a;
    if(a % 2 == 0){
        std::cout << "Even " << std::endl;
    }
    if(a % 2 == 1){
        std::cout << "Odd " << std::endl;
    }

    return 0;
}
