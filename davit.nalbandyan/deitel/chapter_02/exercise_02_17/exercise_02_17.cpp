#include <iostream>

int
main()
{
    std::cout << "a)1 2 3 4" << std::endl;
    std::cout << "b)1 " << "2 " << "3 " << "4" << std::endl;
    std::cout << "c)1 ";
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4 " << std::endl;

    return 0;
}
